# Michelle's ReadMe

This page provides a bit of information about me and my work style so others can get an idea of what it might be like to work with me.
By being intentionally vulnerable, I'm putting my hopes into building trust, especially with those who haven't worked with me before.

Please feel free to contribute to this page by opening a [merge request](https://gitlab.com/michelletorres/readme/-/merge_requests/new).

## What I'm currently working
- [Weekly planner and tracker](https://gitlab.com/michelletorres/planner/-/issues) - Goals of the week, high-level tasks, day-to-day progress, and learnings.


## A little about me
* I'm originally from Guadalajara, Jalisco, Mexico, where I lived my whole life until mid-2019.
* I live in Berlin, Germany. I got to say this was a significant change in my life. To leave my significant other, family, and friends behind and start a new life alone in a new country, new culture, and new language has been a challenge.
* While living in Mexico, I was a teacher at the University (bachelor and master) for a little more than ten years. My most requested class was Web Development, but I also taught Programming 101, Assembly, Java, DB, and other stuff.
* When I'm not working
    * a) I enjoy cooking without following recipes and just letting me go experimenting with new flavors.
    * b) Reading. Sometimes I find myself at 3 am reading without noticing I lost my bedtime. On other occasions, it can be weeks without reading. Side note, I bought a Kindle in 2020, and it is the best self-gift I made in my life.
    * c) Traveling, but due to covid, this is on pause.
    * d) Plant lover. Sometimes I feel like I have no more space at my flat 

To learn even more about me outside work:
- [My usual day](#my-usual-day)
- Trivia

## My communication and working style

Using the [neurodivergent team member profile](https://gitlab.com/gitlab-com/people-group/dib-diversity-inclusion-and-belonging/diversity-and-inclusion/-/blob/master/.gitlab/issue_templates/Team-Member-Profile.md) as inspiration...

### My communication style

- I prefer to communicate through written communications. 
- Sometimes, I struggle with traditional social cues such as eye contact. 
- Silence means that I'm processing information and I appreciate when people respect those silent periods.  

### My working style

- I benefit from work tasks being written or backed up with written communication
- I prefer visual and audio information such as videos and I struggle with long blocks of text. 
- I can handle multiple questions or instructions put to me at one time. 
- I tend to need time to process information before providing answers. 
- I prefer to have information or questions prior to meetings or discussions. 
- I work best when I can context-switch often, I struggle with tasks that requires long periods of concentration.

1. **I like to go with the flow.** 
My brain works best when it's excited about things. If something is a big priority and my brain is not in the mood, I'll play around between tasks to get things done without "overloading" it.  
Example: I'll focus just 20 minutes on this, and then I can get back to that other thing that is more exciting and repeat until it is done.  
Note: This is not hard to achieve because my day is usually an overload of context-switching.  
The downside is that sometimes my brain is "in the zone" at night or weekends. You will get notifications of slack messages, comments, or MRs. But you will also get a notification that I will not be working in the ["normal" schedule](#my-usual-day) the next day.

2. **Having structure is my way of getting things done.**
I need to write things, ideally in bullets.   
My [weekly tracker](https://gitlab.com/michelletorres/readme/-/issues) is an excellent example of the structure I like: What are my goals, what are the tasks to achieve those goals, what have I done, what it's pending, what I learned so far.  

## When reaching out to me ...
My preferences are (in order of preference):
1. Tag me on MR or Issue. I exclusively use [ToDos](https://gitlab.com/dashboard/todos)
1. Assign to me comments on google docs - tagging me is not enough. I exclusively use [followup:actionitems](https://drive.google.com/drive/search?q=followup:actionitems)
1. Tag me on a slack message. I appreciate [transparency](https://about.gitlab.com/handbook/values/#transparency), so please use public channels and avoid DMs

If it's urgent and requires sync:
1. Send me a 1:1 message on slack

Note: _Emails are not listed because I give close to zero priority to this._


## My learning style
* Hands-on - Learn by doing
* Teaching - Learn by sharing
* With examples, examples, and more examples
* Self-directed
* Visual and auditory. Reading is complicated

## At work, nothing makes me happier than
1. When a team executes autonomously and deliver results
1. When a team member learns something that moves them up in their career
1. The smile and the team member's look full of energy and willing after career development conversations
1. When I can apply my last learning to improve a process and benefit the team, department, and company

## Mutual Agreement Aspiration
Following is a list of aspirations I hope that we can agree to as we work together:
1. My dedication as a Manager is to provide servant leadership and support.
1. Your dedication as an Engineer is to work diligently and give your best.
1. Anything we do not know, we will commit to figuring it out.
1. I will never set unrealistic expectations, and I will strive always to communicate openly.
1. You will let me know if you feel expectations have been set too high.
1. I am open to constructive feedback. Please know that I will always work to improve.
1. We will always say what we are going to do and doing what we say.
1. Your struggles will become my struggles; we will work through challenges together and as a team.
1. You will ask for support and let me know if you want me to teach, mentor or coach you.
1. I will always try to explain the "why" so you can focus on the "what" and "how."
1. I am at ease with emotion, and I don't attach shame to tears. We will respect each other's reactions to emotions (e.g., crying) as the release they are.
1. Life can be messy and doesn't stop because we are at work. We will work together to make it a little easier. Please know you can talk about something that is "not work related."

## Strengths
- Derive much satisfaction from doing things that **benefit people** and/or help them improve personally or professionally. Intrigued with the **unique qualities of each person**. Have a good skill in figuring out how people who are different can work together productively.  
- Good at making discoveries and make sense of things, reducing elaborated or intertwined ideas, processes, legal documents, and/or action plans to their basic elements. People usually approach me for **plainspoken, easy-to-understand explanations**.
- **Launching** new ventures thrills me. I'm passionate about moving ideas from the **talking to the action** stage.  
- Tackling **impossible goals** energizes me. Stepping **out of my comfort zone** into unfamiliar territory stimulates me. I like to dive into **challenging situations** and I **trust I can deal with hazards** as I encounter them.  
- Constantly looking to **improve** myself, my team, and the business
- **Honest and candid**. I always open multiple communication channels to facilitate feedback, gather input and adjust my communication or managerial style.


## Weaknesses and mitigation
- I can get **impatient** when I don't have **full control**. For example: asking team members or coworkers for progress
  - Even when I improved a lot, I still sometimes slip on this. My current process is: I share clear expectations since the beginning for different projects or tasks and agree with the team or coworkers on achievable ETAs. Every time I want to ask for an update, I add a reminder for myself with a valid frame according to the ETA.
- It takes some **time** to organize and share my **ideas and arguments** in a **succinct** way. When fast and urgent conversations are happening with challenging and different opinions, I struggle to present my ideas in the same way I get them in my mind.
  - Writing helps a lot. I've been practicing applying answer-first communication for specific situations. The current remote setup is perfect, as many collaboration is async, and I can write documents, share upfront and build my arguments. Practice makes perfect. I'm confident this practice will help me articulate with the same confidence and accuracy during meetings.
- I'm not strong enough at **coding** at the moment
  - I've tried and failed to reserve some time every sprint to work on a ticket from the backlog. This is definitely a weakness that, at the moment, I don't have a clear mitigation plan.   
- I can get **bored or de-energized** easily. Sitting around and **wasting time** does not suit me at all. **Routinary, repetitive and ordinary work** de-energize me.   
  - Allowing me to collaborate and shape the roadmap helps. Get some level of freedom to experiment with my team helps me get engaged. My side work with teaching and speaking also helps.


## My career goals
- I'm **ambitious** but not too much. Director is a role that I don't even know if I want to pursue.
- Gather knowledge of the company and department and build **confidence to perform and meet the expectations of my role**. I expect to have access to coaching, allowing me to continue **developing my strengths** as compensation for my weaknesses. I will like to be involved in projects and connected with sponsors that can give me the exposure to **continue in the leadership path, advancing** the ladder.
- I see myself **shadowing directors** and learning from their experiences to lead a department, prepare budgets and guide engineering teams according to the organizational objectives.

### My usual day

⚠️ As stated in [my working style](#my-working-style), sometimes my brain is "in the zone" at night, which is a factor that the next day I will not have my regular routine.

1. Wake up around 5:30 am and get ready to exercise at home.
1. Exercise routine while listening to leadership, business, or management audiobooks.
1. Around 8:30 am, it is time to get ready to work with a cup of coffee.
1. 9:00 am it's work time. Haha, not always true, and my workday can start at 10:00 am
1. 30 mins to go over To-Dos, pings, and important emails to plan the day.
1. The day will be a continuous context switching between whatever is in my calendar like 1:1s, team meetings, interviewing, etc., and my short and limited focus times to work on my To-Dos.
1. At noon is my sacred lunchtime. It is time to listen to a podcast while cooking whatever new crazy recipe popped in my mind. Eat while watching some series on TV and relax for some minutes on the couch while finishing the chapter.
1. Get back to work and the continuous context switching.
1. Around 3:00 pm is time for a high-protein snack and most probably a cup of coffee or tea.
1. And then, back again to work and the continuous context switching.
1. Around 5:00 pm, I turn off notifications. It is time to wrap up the day, which will typically take around an hour between getting things done, reviewing the list of To-Dos, and collecting the day's learnings.
1. 6:00 pm, or sometimes later, I stop working and jump to whatever activity is good for my mood. From reading, playing Overcooked, watch TV, go out for dinner with friends, or doing chores.
1. At night, when I'm in bed, I do 10 mins sleep meditation and hope to have a good night without insomnia.

