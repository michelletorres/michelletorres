# Ask Me Anything

Is there anything you'd like to know about me? Create an issue, and I'll answer there and record in this file also.

## What are your goals? 1 year out? 5 years out? Lifetime? These could be related or not related to work.
Work:

1 year - to feel comfortable and productive at my role. It is a lot of new things that I need to grab.

5 years - not sure, but probably important to share that I'm not sure if I see myself as director (nothing beyond senior manager), at least that is what I say now. Also, I do not have urgency to go up in the ladder.



Life:

1 year -  still living in Berlin, finally living with my significant other (we've been in a long distance relationship since I moved here)

5 years - living somewhere else in Europe, enjoying the life (no kids) traveling and etc

10 years - most probably living a very relax life similar to retirement or close to it, ideally with a house by the lake or a beach house, or a woods house, hehe.
